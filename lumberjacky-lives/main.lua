
ssx = love.graphics.getWidth()
ssy = love.graphics.getHeight()

fonts = {
	sml = love.graphics.newFont(14),
	med = love.graphics.newFont(28)
}

amplitude = 0.2
frequency = 440
freqMin = 110
freqMax = 1320
length = 1/1024
lengthMin = 1/64
lengthMax = 2
rate = 44100
bits = 16
channels = 1
source = love.audio.newSource(love.sound.newSoundData(length*rate, rate, bits, channels))
iterOffset = 0
sampleRecMax = ssx*4

function love.load()
	sampleRec = {}
	srCtr = 1
	setAudio()
end

function setAudio()
	local soundData = love.sound.newSoundData(length*rate, rate, bits, channels)
	for i=0, length*rate-1 do
		local sample = math.sin((iterOffset+i)/rate*frequency*2*math.pi)*amplitude
		sampleRec[srCtr] = srCtr*(ssx/sampleRecMax)
		srCtr = srCtr % sampleRecMax + 1
		sampleRec[srCtr] = ssy/2+sample*300
		srCtr = srCtr% sampleRecMax + 1
		soundData:setSample(i, sample)
	end
	iterOffset = iterOffset + length*rate
	source = love.audio.newSource(soundData)
	source:play()

	collectgarbage()
end

function love.update(dt)
	local mx, my = love.mouse.getPosition()
	if love.mouse.isDown(1) then
		if my > ssy-50 then
			frequency = math.floor(freqMin+mx/ssx*(freqMax-freqMin))
		elseif my > ssy-100 then
			length = math.floor((lengthMin+mx/ssx*(lengthMax-lengthMin))*32+1)/32
		end
	end
	if not source:isPlaying() then
		setAudio()
	end
end

function love.keypressed(k)
	if k == "escape" then
		love.event.quit()
	elseif k == "r" then
		love.load()
	end
end

function love.draw()
	--waveform
	love.graphics.clear(200, 220, 230)
	love.graphics.setColor(0, 0, 255)
	love.graphics.points(sampleRec)
	--freq slider
	love.graphics.setColor(0, 255, 0, 100)
	love.graphics.rectangle("fill", 0, ssy-50, ssx, 50)
	love.graphics.setColor(0, 255, 0, 200)
	love.graphics.rectangle("fill", 0, ssy-50, ssx*(frequency-freqMin)/(freqMax-freqMin), 50)
	love.graphics.setColor(0, 0, 0)
	love.graphics.setFont(fonts.med)
	local txt = "Frequency: "..frequency
	love.graphics.print(txt, ssx/2-fonts.med:getWidth(txt)/2, ssy-25-fonts.med:getHeight(txt)/2)
	--len slider
	love.graphics.setColor(0, 255, 0, 100)
	love.graphics.rectangle("fill", 0, ssy-100, ssx, 50)
	love.graphics.setColor(0, 255, 0, 200)
	love.graphics.rectangle("fill", 0, ssy-100, ssx*(length-lengthMin)/(lengthMax-lengthMin), 50)
	love.graphics.setColor(0, 0, 0)
	love.graphics.setFont(fonts.med)
	local txt = "Length: "..length
	love.graphics.print(txt, ssx/2-fonts.med:getWidth(txt)/2, ssy-75-fonts.med:getHeight(txt)/2)
	--fps
	love.graphics.setColor(0, 128, 0)
	love.graphics.setFont(fonts.sml)
	love.graphics.print(love.timer.getFPS(), 20, 20)
end
