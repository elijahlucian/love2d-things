# tiny lua

FROM alpine:3.8

RUN \
  apk update && \
  apk add luarocks5.3 && \
  mkdir /scripts

WORKDIR /scripts

VOLUME [ "/scripts" ]

CMD [ "lua5.3" ]

