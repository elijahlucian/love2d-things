function createPlayerObject()

  -- player sprite
  player.x = w / 2
	player.y = h / 2 +160
  yOrigin = player.y - 32
  player.img = love.graphics.newImage('assets/lumberjacky.png')
  player.sprite = love.graphics.newQuad(0,0,8,8, player.img:getDimensions())
	player.ground = player.y

  -- player physics
	player.y_velocity = 0
  player.speed = playerSpeed
	player.jump_height = jumpHeight
	player.gravity = gravity

  return player
end

function createPlayerSounds()

  playerSound.greeting = {}
  playerSound.jump = {}
  playerSound.land = {}

  playerSound.greeting[0] = love.audio.newSource("assets/elijah-penetrate.wav", "static")
  playerSound.greeting[1] = love.audio.newSource("assets/elijah-moist.wav", "static")

  playerSound.land[0] = love.audio.newSource("assets/land01.wav", "static")
  playerSound.land[1] = love.audio.newSource("assets/land02.wav", "static")
  playerSound.land[2] = love.audio.newSource("assets/land03.wav", "static")

  playerSound.jump[0] = love.audio.newSource("assets/jump01.wav", "static")
  playerSound.jump[1] = love.audio.newSource("assets/jump02.wav", "static")
  playerSound.jump[2] = love.audio.newSource("assets/jump03.wav", "static")

  return playerSound
end

function playerAnimate(dt)
  animationTimer = animationTimer - dt
  if animationTimer <= 0 then
    animationTimer = 1 / fps
    frame = frame + 1
    if frame >= numFrames then frame = 0 end
    xOffset = 0 + frame *8
    player.sprite:setViewport(xOffset, 0, 8, 8)
  end
end
