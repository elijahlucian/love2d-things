ssx = love.graphics.getWidth()
ssy = love.graphics.getHeight()

amplitude = 0.2
frequency = 440
freqMin = 220
freqMax = 1320
length = 100
lengthMin = 1/64
lengthMax = 2
rate = 44100
bits = 16
channels = 1
source = love.audio.newSource(love.sound.newSoundData(length*rate, rate, bits, channels))
iterOffset = 0
sampleRecMax = ssx*4

function love.load()

  sampleRec = {}
	srCtr = 1
	setAudio()

  h = love.graphics.getHeight()
  w = love.graphics.getWidth()

  fm = {}
  fm.mod = 1

  l = {}
  l.x = w / 2
  l.y = h / 2
  l.starting_angle = 1
  l.angle = l.starting_angle
  l.len = 30
  l.speed = 10

  l.rgb = {1,1,1}

  canvas = love.graphics.newCanvas(w,h)
end

function paint_the_canvas()

end

function setAudio()
	local soundData = love.sound.newSoundData(length*rate, rate, bits, channels)

  for i=0, length*rate-1 do
		local sample = math.sin((iterOffset+i)/rate*frequency*2*math.pi)*amplitude
		sampleRec[srCtr] = srCtr*(ssx/sampleRecMax)
		srCtr = srCtr % sampleRecMax + 1
		sampleRec[srCtr] = ssy/2+sample*300
		srCtr = srCtr% sampleRecMax + 1
		soundData:setSample(i, sample)
	end
	iterOffset = iterOffset + length*rate
	source = love.audio.newSource(soundData)
	source:play()

	collectgarbage()
end

function love.update(dt)
  -- l.x = l.x + 0 + love.math.random(1)
  -- l.y = l.y + 0 + love.math.random(1)

  if l.angle > 179 then
    l.x = love.math.random(w)
    l.y = love.math.random(h)
    l.angle = 1
  end
  l.angle = l.angle + ( 1 * l.speed )

  source:setPitch(1*math.cos(l.angle/180*3.14)+1)
  fm.mod = fm.mod + 0.01
  -- if not source:isPlaying() then
  --   setAudio()
  -- end

  if l.x > w then
    l.x = 0
    l.y = love.math.random(w)
  end
  if l.y > h then
    l.y = 0
    l.x = love.math.random(h)
  end

  l.x0 = l.x-l.len*math.cos(l.angle/180*3.14)
  l.y0 = l.y-l.len*math.sin(l.angle/180*3.14)
  l.x1 = l.len*math.cos(l.angle/180*3.14)+l.x
  l.y1 = l.len*math.sin(l.angle/180*3.14)+l.y

  for i=1, 3 do
    if l.rgb[i] > 0 then l.rgb[i] = l.rgb[i] - 0.001*i else l.rgb[i] = 1 end
  end
end

function love.draw()
  -- love.graphics.setColor(255, 255, 255, 255)
  -- love.graphics.setBlendMode("alpha", "premultiplied")
  love.graphics.setCanvas(canvas)
    love.graphics.setColor(unpack(l.rgb))
    love.graphics.line(l.x0,l.y0,l.x1,l.y1)
    -- for i=1, 3 do
    --   love.graphics.print(l.rgb[i],10,i*20)
    -- end
  love.graphics.setCanvas()
  love.graphics.draw(canvas, 0,0)

  love.graphics.print(fm.mod)
end
