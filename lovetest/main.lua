g = love.graphics


function love.load()

  paused = false

  w = g.getWidth()
  h = g.getHeight()

  planet = {}
  planet.x = w/2
  planet.y = h/2
  planet.rad = 5
  planet.color = {0.4,0.8,0.7}

  shadow = {}

  speed = 30
  t = 1
end

function love.update(dt)
  if love.keyboard.isDown('space') then paused = true end

  planet.x = (w/2 - 100) * math.cos(t*speed/180*6.28) + w/2
  planet.y = h/8 * math.sin(t*speed/180*6.28) + h/2
  planet.rad = planet.y/8
  t = t + dt

  shadow.middle = (w-100)/2
  shadow.x = planet.x - shadow.middle

end

function draw_planet()
  g.setColor(unpack(planet.color))
  g.circle('fill', planet.x, planet.y, planet.rad)

end

function draw_shadow()

  g.setColor(0.2,0.6,0.4)
  g.circle('fill', planet.x, planet.y, planet.rad)
  -- g.ellipse('fill', planet.x, 3*(h/4), planet.rad, planet.rad)

end

function draw_sun()
  g.setColor(0.8,0.9,0.4)
  g.circle('fill', w/2, h/2, 90)
end


function love.draw()
  if planet.y > h/2 then
    draw_sun()
    draw_planet()
    draw_shadow()
  else
    draw_planet()
    draw_sun()
    -- draw_shadow()
  end
  g.setColor(1,1,1)
  g.print(w, 10, 10)
  g.print(h, 10, 30)
  g.print(shadow.x, 10, 50)

end
