BadGuy = {}

function BadGuy.new()
  -- add ability for levels
  
  self = {}

  self.name = "Ralph"
  self.x = love.math.random(0, config.edge_right)
  self.y = love.math.random(0, config.edge_bottom)
  self.v = {
    x = 0,
    y = 0,
    length = 0,
    rad = 0,
    speed = 0.1,
  }

  self.size = math.random(300,600)/100
  self.moving = "nah"
  self.life = 100

  self.angle_timer = 0
  self.angle_interval = 0.1
  self.next_angle_read = self.angle_interval

  self.shot_timer = 0
  self.shot_interval = love.math.random(1000,4500)/1000
  self.next_shot = self.shot_interval

  self.approach = function(self)
    local x_dir = 1
    local y_dir = 1
    if self.v.x < 0 then x_dir = -1 end
    if self.v.y < 0 then y_dir = -1 end
  
    local xx = self.x + (self.v.speed * math.sin(self.v.rad)) * x_dir
    local yy = self.y + (self.v.speed * math.cos(self.v.rad)) * y_dir
    self.x = xx
    self.y = yy

    -- if self.v_speed < 10 then self.v_speed = self.v_speed + 0.1 end
  end

  self.update = function(self, dt, m)
    self.angle_timer = self.angle_timer + dt
    self.shot_timer = self.shot_timer + dt

    if self.angle_timer > self.next_angle_read then
      self.next_angle_read = self.next_angle_read + self.angle_interval
      self = utils.get_vector(m, self)
    end

    if self.v.length > 20 then
      self:approach()
      self.moving = "yeah"
    elseif self.v.length <= 20 and self.moving == "yeah" then
      self.moving = "nah"
    end
  end

  self.draw = function(self)
    love.graphics.circle("fill", self.x, self.y, self.size)
  end

  return self
end

return BadGuy
