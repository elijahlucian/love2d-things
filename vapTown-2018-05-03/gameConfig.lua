-- init all the shit

math.randomseed(os.time())

-- player objects

platform = {}
player = {}
playerSound = {}

-- npc objects

npc = {}
villager = {}
soundboard = {}
npcNames = {}

mob = {}

-- world objects

worldLog = {}
worldPinecone = {}

-- soundboard shit
dir = "soundboard"
totalFiles = 0

--animation
fps = 8
animationTimer = 1 / fps
frame = 0
numFrames = 2
xOffset = 0
rnd = math.random(0,2)

-- shit close to player and shit
playerInAir = 0
oldDT = 0
newDT = 0
nearestVillager = nil
playerDirection = 1

-- world boundaries
w,h = love.graphics.getWidth(),love.graphics.getHeight()
horizon = h/2 - 32
boundXmin, boundXmax = -500, w+700
boundYmin, boundYmax = -500, h + 500

-- player constants
playerSpeed = 350
gravity = -1800
jumpHeight = -600
yOrigin = nil
conversationDistance = 20
eventHorizon = h-215

-- colors
-- https://coolors.co/292f36-0b7a75-d7c9aa-7b2d26-f0f3f5
sky = {240,243,245}
ground = {41,47,54}
guiColor = {123,45,48}

function loadConfigs()
  -- graphics settngs
  love.graphics.setDefaultFilter("nearest","nearest")
  font = love.graphics.newFont("assets/font01.ttf", 18)
  love.graphics.setFont(font)

  -- platform
  platform.width = w
  platform.height = h
  platform.x = 0
  platform.y = platform.height / 2
end
