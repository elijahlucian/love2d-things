-- requires are currently just creating global shit.

require "soundboard" -- getSoundboard()
require 'gameState' -- inventory and shit
require 'gameConfig' -- loadConfigs()

-- game sounds
require 'gameSounds' -- getSoundboard(dir), playVillagerSound()

-- include game object functions
require 'playerObject' -- createPlayerObject(), createPlayerSounds(), playerAnimate(dt)
require 'villagerObject' -- makeVillager(npcName), resetVillagerPosition(), villagerAnimate(dt), showVillagerName(dt)
require 'worldLog' --makeLogs(), --flyLogs()

-- include game dialog functions
require 'gameControls' -- moveVillager(), moveVillagerUpDown()
require 'gameDialog' -- getNearestVillager()

-- init soundboard.
files = getSoundboard(dir) -- in gameSounds.lua

function love.load()
  bgm = love.audio.newSource("eric-towntheme.wav", "stream")
  bgm:setVolume(0.2)
  ambience = love.audio.newSource("openair-summerday.wav", "stream")
  loadConfigs() -- in gameConfig.lua
  createPlayerObject() -- in playerObject.lua
  createPlayerSounds() -- in playerObject.lua
  for i = 1, #npcNames do table.insert(villager, makeVillager(npcNames[i])) end  -- generate villagers
end

function love.update(dt)

  getNearestVillager() -- in gameControls.lua
  showVillagerName(dt) -- in villagerObject.lua

  if love.keyboard.isDown('w') then
    moveVillagerUpDown(dt,1)
    playerAnimate(dt)
  elseif love.keyboard.isDown('s') then
    moveVillagerUpDown(dt,-1)
    playerAnimate(dt)
  end

  -- move player right
	if love.keyboard.isDown('d') then
    if player.x < w-250 then
    	if player.x < (love.graphics.getWidth() - player.img:getWidth()) then
  			player.x = player.x + (player.speed * dt)
      end
    else
      moveVillager(dt,1)
    end
    playerAnimate(dt)

  -- move player lef
	elseif love.keyboard.isDown('a') then
    if player.x > 250 then
    	if player.x > 0 then
  			player.x = player.x - (player.speed * dt)
      end
    else
      moveVillager(dt,-1)
    end

    playerAnimate(dt)
	end

  if love.keyboard.isDown('e') then
    villagerAnimate(dt)
  end

	if love.keyboard.isDown('space') then
    if player.y_velocity == 0 then
      rnd = math.random(0,2)
      love.audio.play(playerSound.jump[rnd])
			player.y_velocity = player.jump_height
      playerInAir = 1
    end
    if player.y_velocity == 0 then
      playerInAir = 0
    end
	end

  -- collider
	if player.y_velocity ~= 0 then
		player.y = player.y + player.y_velocity * dt
		player.y_velocity = player.y_velocity - player.gravity * dt
	end

	if player.y > player.ground then
		player.y_velocity = 0
    player.y = player.ground
    love.audio.play(playerSound.land[rnd])
	end
-- end redraw funciton
end

function drawVillager(v)

  -- function to draw the villger
  if nearestVillager == nil then
    -- shit is fucked
  else
    love.graphics.setColor(guiColor)
    love.graphics.print(nearestVillager.msg,15, 15,0,2,2)
  end
  -- villager shit
  love.graphics.setColor(255,255,255,255)
  love.graphics.draw(v.imgBody, v.spriteBody, v.x, v.y, 0,v.ratio,v.ratio,0,0)
  love.graphics.setColor(v.color)
  love.graphics.draw(v.imgClothes, v.spriteClothes, v.x, v.y, 0,v.ratio,v.ratio,0,0)
end

function love.draw()

  -- ground
  love.graphics.setColor(ground)
	love.graphics.rectangle('fill', platform.x, platform.y, platform.width, platform.height)
  local playerOrigin = player.y - 55

  -- villagers in background
  for vil in ipairs(villager) do
    local v = villager[vil]
    if v.y <= playerOrigin then
      drawVillager(v)
    end
  end

  -- player shadow
  love.graphics.setColor(0,0,0,30)
  love.graphics.ellipse("fill", player.x+32, eventHorizon, 30+((player.y-yOrigin)/4), 10+((player.y-yOrigin)/8), 15)

  --  player
  love.graphics.setColor(255,255,255,255)
	love.graphics.draw(player.img,player.sprite, player.x, player.y, 0, 8, 8, 0, 7)

  -- villagers in foreground
  for vil in ipairs(villager) do
    local v = villager[vil]
    if v.y > playerOrigin then
      drawVillager(v)
    end
  end

  -- background
  love.graphics.setBackgroundColor(sky)

  -- dialog
  love.graphics.setColor(11,122, 117)
  love.graphics.print("lumberJacky", player.x-10, player.y-90)
  love.audio.play(bgm)
  love.audio.play(ambience)
end
