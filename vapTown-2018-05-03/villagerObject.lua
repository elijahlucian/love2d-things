function makeVillager(npcName)
  local v = {}

  local nameIndex = {"guy","bro","dude","chick","man","lady","person","thing","beast"}
  local n = "Talk To: " .. npcName .. "" .. nameIndex[math.random(1, #nameIndex)]

  v.name = npcName
  v.msg = n
  v.x = math.random(boundXmin, boundXmax)
  v.y = h/2 + math.random(0,300) -- random in bg and foreg - change scale depending on behind or in front

  local perspectiveScale = eventHorizon - v.y + 150
  goldenRatio = (1/ (perspectiveScale) *200)*4 +2
  --print(goldenRatio)

  v.ratio = goldenRatio

  -- ellie skin
  v.imgBody = love.graphics.newImage('assets/ellie-split-skin.png')
  v.spriteBody = love.graphics.newQuad(0, 0, 8, 8, player.img:getDimensions())

  -- ellie dress
  v.imgClothes = love.graphics.newImage('assets/ellie-split-dress-hair.png')
  v.spriteClothes = love.graphics.newQuad(0, 0, 8, 8, player.img:getDimensions())

  v.ground = v.y
  v.color = {math.random(50, 255), math.random(50, 255), math.random(50, 255)}

  -- load soundbank for villager
  v.sound = {}
  local dir = "soundboard/" .. v.name
  local folderContents = love.filesystem.getDirectoryItems(dir)
  v.soundlength = #folderContents

  for k,x in ipairs(folderContents) do
    local loadSound = dir .. "/" .. x
    -- print(loadSound)
    theSound = love.audio.newSource(loadSound, "stream")
    table.insert(v.sound, theSound)
  end
  return v
end

function resetVillagerPosition()
  for vil in ipairs(villager) do
    local v = villager[vil]
    v.x = math.random(boundXmin, boundXmax)
    v.y = h/2 + math.random(0,300)
    local perspectiveScale = eventHorizon - v.y + 150
    goldenRatio = (1/ (perspectiveScale) *200)*4 +2
    v.ratio = goldenRatio
  end
end

-- animate villagers
function villagerAnimate(dt)
  if nearestVillager == nil then print("no villager to animate") else
    animationTimer = animationTimer - dt
    if animationTimer <= 0 then
      animationTimer = 0.5 / fps
      frame = frame + 1
      if frame >= numFrames then frame = 0 end
      xOffset = 0 + frame *8
      nearestVillager.spriteBody:setViewport(xOffset, 0, 8, 8)
    end
  end
end

-- update functions
function showVillagerName(dt)
  newDT = newDT + dt
  if newDT >= oldDT + 2 then
    if nearestVillager == nil then print("nil table") else print(nearestVillager.msg) end
    oldDT = newDT
  end
end
