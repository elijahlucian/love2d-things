gameState = {
  -- currency and consumables
  logs = 0,
  potions = 0,

  -- upgrades
  backpack = 0,
  axe = 0,
  sword = 0,

  -- experience and levels (maybe)
  level = 1,
  experience = 0,

  -- game events?
  storyProgress = 0,


}

return gameState
