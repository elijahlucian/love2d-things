-- get nearest villager
function getNearestVillager()
  -- loop thru and find nearest villager
  local foundOne = 0
  for vil in ipairs(villager) do
    local v = villager[vil]
    local distanceFromPlayer = player.x - v.x
    local currentDistance = v.x - player.x
    local currentPlane = v.y - yOrigin

    if math.abs(currentDistance) < conversationDistance then
      if math.abs(currentPlane) < conversationDistance/4 then
        foundOne = 1
        nearestVillager = v
        break
      end
    end
  end
  if foundOne == 0 then nearestVillager = nil end
end
