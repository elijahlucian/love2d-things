BadGuy = {}

function BadGuy.new()
  -- add ability for levels
  
  self = {}

  self.name = "Ralph"
  self.x = love.math.random(0, config.edge_right)
  self.y = love.math.random(0, config.edge_bottom)
  self.x_dir = 1
  self.y_dir = 1
  self.v = {
    x = 0,
    y = 0,
    length = 0,
    rad = 0,
    speed = 0.1,
  }

  self.size = math.random(400,550)/100
  self.moving = "nah"
  self.life = 100

  self.angle_timer = 0
  self.angle_interval = 0.1
  self.next_angle_read = self.angle_interval

  self.shot_timer = 0
  self.shot_interval = love.math.random(1000,4500)/1000
  self.next_shot = self.shot_interval

  self.approach = function(self)
    if self.v.x < 0 then self.x_dir = -1 else self.x_dir = 1 end
    if self.v.y < 0 then self.y_dir = -1 else self.y_dir = 1 end
  
    self.x = self.x + (self.v.speed * math.sin(self.v.rad)) * self.x_dir
    self.y = self.y + (self.v.speed * math.cos(self.v.rad)) * self.y_dir

    -- if self.v_speed < 10 then self.v_speed = self.v_speed + 0.1 end
  end

  self.update = function(self, dt, m)
    self.angle_timer = self.angle_timer + dt
    self.shot_timer = self.shot_timer + dt

    if self.angle_timer > self.next_angle_read then
      self.next_angle_read = self.next_angle_read + self.angle_interval
      self = utils.get_vector(m, self)
    end

    if self.v.x < 0 then self.v.x_dir = -1 else self.v.x_dir = 1 end
    if self.v.y < 0 then self.v.y_dir = -1 else self.v.y_dir = 1 end

    if self.v.length > 20 then
      self:approach()
      self.moving = "yeah"
    elseif self.v.length <= 20 and self.moving == "yeah" then
      self.moving = "nah"
    end
  end

  self.draw = function(self)
    
    -- love.graphics.circle("fill", self.x, self.y, self.size)
    love.graphics.draw(ellie.img, ellie.sprite, self.x, self.y, 0, self.size * self.v.x_dir, self.size, ellie.size.width/self.size, ellie.size.height/self.size)
  end

  return self
end

return BadGuy
