
config = {
  movement_speed = 2,
  edge_right = love.graphics.getWidth(),
  edge_bottom = love.graphics.getHeight(),
  bullets = {
    interval = 2,
  },
  bad_guys = {
    max = 5,
  },
}

bad_guys = {}
bullets = {}

-- seed = os.time()
-- math.randomseed(seed)

time = 0
next_bullet = config.bullets.interval

utils = require("utils")
move = require("move")
Bullet = require("Bullet")
BadGuy = require("BadGuy")

for i = 1, config.bad_guys.max do
  table.insert(bad_guys, BadGuy.new())
end

m = {
  x = 0,
  y = 0,
  size = 5,
}

function p(body,x,y)
  return love.graphics.print(body,x,y)
end

function love.load()
  love.mouse.setVisible(false)
  font = love.graphics.newFont("assets/font.ttf",14)
end

function love.keyreleased(key)
  if key == "esc" then
    love.event.quit()
  end
end

function love.update(dt)
  mX,mY = love.mouse.getPosition()
  m.x = mX
  m.y = mY
  time = time + dt  

  for i, bad_guy in ipairs(bad_guys) do
    bad_guy:update(dt,m)
    if bad_guy.shot_timer > bad_guy.next_shot then
      bad_guy.next_shot = bad_guy.next_shot + bad_guy.shot_interval
      table.insert(bullets, Bullet.new(bad_guy.x, bad_guy.y, bad_guy.v.x, bad_guy.v.y, bad_guy.v.rad))
    end

  end

  -- idea: iterate over all the objects in the table, and use the index as the divisor
  -- with each frame, so it passes all the processing onto separate frames
  for i,bullet in ipairs(bullets) do
    bullet:update(dt)
    if bullet.life < 0 then
      table.remove(bullets, i)
    end
  end
  -- idea: calculate angles every N seconds.

  -- if t1.v_length > 5 then
  --   move.approach(t1)
  -- else
  --   t1.v_speed = 1
  -- end
  -- move.approach(m, t2, speed)
end

function love.draw()
  
  -- t2 = utils.get_vector(m,t2)

  love.graphics.circle("fill", m.x, m.y, m.size)

  for i, bad_guy in ipairs(bad_guys) do
    
    -- p("bad_guy_"..i, i*100, 10, 0)
    -- p(bad_guy.v.length, i*100, 20)
    -- p(math.deg(bad_guy.v.rad), i*100, 30)
    -- p(bad_guy.v.x, i*100, 40)
    -- p(bad_guy.v.y, i*100, 50)
    -- p(bad_guy.v.speed, i*100, 60)
    -- p(bad_guy.shot_timer, i*100, 70)
    -- p(bad_guy.shot_interval, i*100, 80)

    bad_guy:draw()
  end 
  
  p("Total Bullets: "..table.getn(bullets), config.edge_right - 120, 0)
  
  -- love.graphics.line(t1.x,t1.y,t1.x+t1.v_x,t1.y+t1.v_y)
  -- love.graphics.line(t1.x,t1.y,t1.x,m.y)
  -- love.graphics.line(t1.x,m.y,m.x,m.y)
  
  for i,bullet in ipairs(bullets) do
    bullet:draw()
  end

end
