config = {
  movement_speed = 2,
  edge_right = love.graphics.getWidth(),
  edge_bottom = love.graphics.getHeight(),
  bullets = {
    interval = 2,
  },
  bad_guys = {
    max = 5,
  },
}

ddd = ""

bad_guys = {}
bullets = {}

player = {}
player_bullets = {}

time = 0
next_bullet = config.bullets.interval

utils = require("utils")
move = require("move")
Bullet = require("Bullet")
BadGuy = require("BadGuy")

for i = 1, config.bad_guys.max do
  table.insert(bad_guys, BadGuy.new())
end

m = {
  x = love.graphics.getWidth() / 2,
  y = love.graphics.getHeight() / 2,
  size = 5,
}

ellie = {}

function p(body,x,y)
  return love.graphics.print(body,x,y)
end

function love.load()
  love.graphics.setDefaultFilter("nearest","nearest")

  ellie.img = love.graphics.newImage("assets/ellie.png")
  ellie.sprite = love.graphics.newQuad(0,0,8,8, ellie.img:getDimensions())
  ellie.size = {
    width = ellie.img:getWidth(),
    height = ellie.img:getHeight(),
  }


  love.mouse.setVisible(false)
  font = love.graphics.newFont("assets/font.ttf",14)
  love.graphics.setFont(font)
end

function love.keyreleased(key)
  if key == "esc" then
    love.event.quit()
  end
end

function controls(dt)

  if love.keyboard.isDown('w') then
    ddd = "up"
    m.x = m.x - dt
  elseif love.keyboard.isDown('s') then
    ddd = "down"
    m.x = m.x + dt
  end

  if love.keyboard.isDown('a') then
    ddd = "left"
    m.y = m.y - dt
  elseif love.keyboard.isDown('d') then
    ddd = "right"
    m.y = m.y + dt
  end

end

function love.update(dt)

  controls(dt)

  -- mX,mY = love.mouse.getPosition()
  -- m.x = mX
  -- m.y = mY
  time = time + dt

  for i, bad_guy in ipairs(bad_guys) do
    bad_guy:update(dt,m)
    if bad_guy.shot_timer > bad_guy.next_shot then
      bad_guy.next_shot = bad_guy.next_shot + bad_guy.shot_interval
      table.insert(bullets, Bullet.new(bad_guy.x, bad_guy.y, bad_guy.v.x, bad_guy.v.y, bad_guy.v.rad))
    end
  end

  -- idea: iterate over all the objects in the table, and use the index as the divisor
  -- with each frame, so it passes all the processing onto separate frames

  for i,bullet in ipairs(bullets) do
    bullet:update(dt)
    if bullet.life < 0 then
      table.remove(bullets, i)
    end
  end

end

function love.draw()

  love.graphics.circle("fill", m.x, m.y, m.size)

  for i, bad_guy in ipairs(bad_guys) do
    bad_guy:draw()
  end

  for i,bullet in ipairs(bullets) do
    bullet:draw()
    love.graphics.setColor(1,1,1)
  end

  love.graphics.print("Total Bullets: "..table.getn(bullets), config.edge_right - 120, 0)
  love.graphics.print(ddd, config.edge_right - 120, 10)
end
