Bullet = {}

function Bullet.new(x, y, v_x, v_y, rad)
  local self = {}

  -- bullet color randomization
  self.x = x
  self.y = y
  self.v = {
    rad = rad,
    speed = 300,
  }

  self.color = {love.math.random(30,100)/100, love.math.random(0,60)/100, love.math.random(50,70)/100}

  if v_x < 0 then self.v.x_dir = -1 else self.v.x_dir = 1 end
  if v_y < 0 then self.v.y_dir = -1 else self.v.y_dir = 1 end

  self.v.x = math.sin(self.v.rad) * self.v.x_dir
  self.v.y = math.cos(self.v.rad) * self.v.y_dir

  self.size = {
    x = love.math.random(300,400)/100,
    y = love.math.random(300,400)/100,
  }

  self.life = 2

  self.check_bounds = function(self)
  end

  self.update = function(self,dt) 
    self.x = self.x + (self.v.x * dt * self.v.speed)
    self.y = self.y + (self.v.y * dt * self.v.speed)
    self.life = self.life - dt
  end

  self.draw = function(self)
    love.graphics.setColor(self.color)
    love.graphics.ellipse("fill", self.x, self.y, self.size.x, self.size.y)
    
  end

  return self

end

return Bullet
