local move = {}

function move.approach(t)
  local x_dir = 1
  local y_dir = 1
  if t.v.x < 0 then x_dir = -1 end
  if t.v.y < 0 then y_dir = -1 end

  local xx = t.x + (t.v.speed * math.sin(t.v.rad)) * x_dir
  local yy = t.y + (t.v.speed * math.cos(t.v.rad)) * y_dir
  
  t.x = xx
  t.y = yy
  -- if t.v_speed < 10 then t.v_speed = t.v_speed + 0.1 end
  return t
end

return move
