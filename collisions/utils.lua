local utils = {}

function utils.get_length(length)
  return math.sqrt(length)
end

function utils.fif(condition, if_true, if_false)
  if condition then return if_true else return if_false end
end

function utils.abs(m, t)
  return math.abs(t-m)
end

function utils.get_vector(m, t)
  t.v.x = m.x - t.x
  t.v.y = m.y - t.y
  t.v.length = math.sqrt(t.v.x^2 + t.v.y^2)
  t.v.rad = math.acos(math.abs(t.v.y)/t.v.length)
  return t
end

return utils
