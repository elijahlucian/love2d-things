platform = {}
player = {}
npc = {}

dir = "C:/soundboard"

love.filesystem.getDirectoryItems(dir)

-- states
local fps = 8
local animationTimer = 1 / fps
local frame = 0
local numFrames = 2
local xOffset
local rnd = math.random(0,2)
local playerInAir

local w,h = love.graphics.getWidth(),love.graphics.getHeight()

-- player constants
local playerSpeed = 350
local gravity = -1800
local jumpHeight = -600
local yOrigin

-- colors
local sky = {240,243,245}
local ground = {41,47,54}

-- https://coolors.co/292f36-0b7a75-d7c9aa-7b2d26-f0f3f5

function love.load()
  -- graphics settngs
  love.graphics.setDefaultFilter("nearest","nearest")
  font = love.graphics.newFont("font01.ttf", 18)
  love.graphics.setFont(font)

  -- platform
  platform.width = w
	platform.height = h

	platform.x = 0
	platform.y = platform.height / 2

  -- player sprite
  player.x = w / 2
	player.y = h / 2 +75
  yOrigin = player.y
  player.img = love.graphics.newImage('lumberjacky.png')
  player.sprite = love.graphics.newQuad(0,0,8,8, player.img:getDimensions())
	player.ground = player.y

  -- npc sprite
  npc.x = math.random(0, w)
  npc.y = h/2 + 75
  npc.img = love.graphics.newImage('ellie.png')
  npc.sprite = love.graphics.newQuad(0, 0, 8, 8, player.img:getDimensions())
  npc.ground = npc.y

  -- player physics
	player.y_velocity = 0
  player.speed = playerSpeed
	player.jump_height = jumpHeight
	player.gravity = gravity

  -- soundbank
  sound = {}
  sound.greeting = {}
  sound.jump = {}

  sound.greeting[0] = love.audio.newSource("elijah-penetrate.wav", "static")
  sound.greeting[1] = love.audio.newSource("elijah-moist.wav", "static")

  sound.jump[0] = love.audio.newSource("jump01.wav", "static")
  sound.jump[1] = love.audio.newSource("jump02.wav", "static")
  sound.jump[2] = love.audio.newSource("jump03.wav", "static")
end

function playerAnimate(dt)
  animationTimer = animationTimer - dt
  if animationTimer <= 0 then
    animationTimer = 1 / fps
    frame = frame + 1
    if frame >= numFrames then frame = 0 end
    xOffset = 0 + frame *8
    player.sprite:setViewport(xOffset, 0, 8, 8)
  end
end

function love.keyreleased(key)
    if key == "escape" then
      love.event.quit()
    elseif key == "a" or "d" then
      player.sprite:setViewport(0, 0, 8, 8)
    end
end

function love.keypressed(key)
  if key == "p" then
    love.audio.play(sound.greeting[0])
  elseif key == "m" then
    love.audio.play(sound.greeting[1])
  end
end

function love.update(dt)
	if love.keyboard.isDown('d') then
		if player.x < (love.graphics.getWidth() - player.img:getWidth()) then
			player.x = player.x + (player.speed * dt)
      playerAnimate(dt)
		end
	elseif love.keyboard.isDown('a') then
		if player.x > 0 then
			player.x = player.x - (player.speed * dt)
      playerAnimate(dt)
		end
	end

	if love.keyboard.isDown('space') then
    if player.y_velocity == 0 then
      rnd = math.random(0,2)
      love.audio.play(sound.jump[rnd])
			player.y_velocity = player.jump_height
      playerInAir = 1
    end

    if player.y_velocity == 0 then
      playerInAir = 0
    end

	end

  --collider

	if player.y_velocity ~= 0 then
		player.y = player.y + player.y_velocity * dt
		player.y_velocity = player.y_velocity - player.gravity * dt
	end
	if player.y > player.ground then
		player.y_velocity = 0
    	player.y = player.ground
	end
-- end redraw funciton
end

function love.draw()
  -- ground
  love.graphics.setColor(ground)
	love.graphics.rectangle('fill', platform.x, platform.y, platform.width, platform.height)

  -- player shadow
  love.graphics.setColor(0,0,0,30)
  love.graphics.ellipse("fill", player.x+32, platform.height-215, 30+((player.y-yOrigin)/4), 10+((player.y-yOrigin)/8), 15)

  --  player
  love.graphics.setColor(255,255,255,255)
	love.graphics.draw(player.img,player.sprite, player.x, player.y, 0, 8, 8, 0, 7)

  -- npcs
  love.graphics.draw(npc.img,npc.sprite, npc.x, npc.y, 0, 8, 8, 0, 7)

  -- background
  love.graphics.setBackgroundColor(sky)

  -- dialog
  love.graphics.setColor(11,122, 117)
  love.graphics.print("player 1", player.x-10, player.y-90)
end
