
-- game controls
function love.keypressed(key, scancode, isrepeat)
  -- body...
end

function love.keyreleased(key)
    if key == "escape" then
      love.event.quit()
    elseif key == "r" then
      resetVillagerPosition()
    elseif key =="e" then
      playVillagerSound()
      print("dafuq")
      -- villagerAnimate(dt)
    elseif key == "a" or "d" or "w" or "s" then
      player.sprite:setViewport(0, 0, 8, 8)
    elseif key == "e" then
      nearestVillager.spriteBody:setViewport(0, 0, 8, 8)
    end
end

-- play funny sounds
function love.keypressed(key)
  if key == "p" then
    love.audio.play(playerSound.greeting[0])
  elseif key == "m" then
    love.audio.play(playerSound.greeting[1])
  end
end

-- world movement x axis
function moveVillager(dt,direction)
  for vil in ipairs(villager) do
    local v = villager[vil]
    v.x = v.x - ((playerSpeed/2) * dt * direction * (v.ratio/4))
    if v.x < boundXmin-20 then v.x = boundXmax end
    if v.x > boundXmax+20 then v.x = boundXmin end
  end
end

-- world movement y axis
function moveVillagerUpDown(dt,direction)
  for vil in ipairs(villager) do
    local v = villager[vil]
    v.y = v.y + ((playerSpeed/4) * dt * direction)
    if v.y < horizon then v.y = h-100 end
    if v.y > h-100 then v.y = horizon end
    local perspectiveScale = eventHorizon - v.y + 150
    goldenRatio = (1/ (perspectiveScale) *200)*4 +2
    v.ratio = goldenRatio
  end
end
